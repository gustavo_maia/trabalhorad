
package br.com.larimaia.bo;

import br.com.larimaia.dao.EnderecoDAO;
import br.com.larimaia.model.Endereco;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class EnderecoBO {
    private EnderecoDAO enderecoDAO;

    public EnderecoBO() {
        enderecoDAO = new EnderecoDAO();
    }

    public EnderecoBO(EnderecoDAO enderecoDAO) {
        this.enderecoDAO = enderecoDAO;
    }
    
    public Integer salvar(Endereco produto) throws ServiceException {

        if (produto.getNumero() == null) {
            throw new ServiceException("Campo Número é obrigatório!");
        }

        if (produto.getBairro().isEmpty()) {
            throw new ServiceException("Campo bairro é obrigatório!");
        }

        if (produto.getCidade().isEmpty()) {
            throw new ServiceException("Campo cidade é obrigatório!");
        }
        
        if (produto.getRua().isEmpty()) {
            throw new ServiceException("Campo rua é obrigatório!");
        }
        
        if (produto.getEstado().isEmpty()) {
            throw new ServiceException("Campo estado é obrigatório!");
        }
        int i = 0;
        i = enderecoDAO.salvar(produto);
        return i;
    }

    public void excluir(Integer id) {
        enderecoDAO.excluir(id);
    }

    public Endereco buscarPorId(Integer id) {
        return enderecoDAO.buscarPorId(id);
    }

    public List<Endereco> buscarTodos() {
        return enderecoDAO.buscarTodos();
    }
}
