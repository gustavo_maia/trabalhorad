
package br.com.larimaia.bo;

import br.com.larimaia.dao.ProdutoDAO;
import br.com.larimaia.model.Produto;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class ProdutoBO {
    private ProdutoDAO produtoDAO;

    public ProdutoBO() {
        produtoDAO = new ProdutoDAO();
    }

    public ProdutoBO(ProdutoDAO produtoDAO) {
        this.produtoDAO = produtoDAO;
    }
    
    public void salvar(Produto produto) throws ServiceException {

        if (produto.getDescricao().isEmpty()) {
            throw new ServiceException("Campo descrição é obrigatório!");
        }

        if (produto.getValor().isNaN()) {
            throw new ServiceException("Campo telefone é obrigatório!");
        }

        produtoDAO.salvar(produto);

    }

    public void excluir(Integer id) {
        produtoDAO.excluir(id);
    }

    public Produto buscarPorId(Integer id) {
        return produtoDAO.buscarPorId(id);
    }

    public List<Produto> buscarTodos() {
        return produtoDAO.buscarTodos();
    }
}
