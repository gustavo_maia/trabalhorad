
package br.com.larimaia.bo;

import br.com.larimaia.dao.TipoEventoDAO;
import br.com.larimaia.model.TipoEvento;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class TipoEventoBO {
    
    private TipoEventoDAO tipoEventoDAO;

    public TipoEventoBO() {
        tipoEventoDAO = new TipoEventoDAO();
    }

    public TipoEventoBO(TipoEventoDAO tipoEventoDAO) {
        this.tipoEventoDAO = tipoEventoDAO;
    }
    
    public void salvar(TipoEvento tipoEvento) throws ServiceException {

        if (tipoEvento.getDescricao().isEmpty()) {
            throw new ServiceException("Campo descrição é obrigatório!");
        }

        tipoEventoDAO.salvar(tipoEvento);

    }

    public void excluir(Integer id) {
        tipoEventoDAO.excluir(id);
    }

    public TipoEvento buscarPorId(Integer id) {
        return tipoEventoDAO.buscarPorId(id);
    }

    public List<TipoEvento> buscarTodos() {
            return tipoEventoDAO.buscarTodos();
    }
}
