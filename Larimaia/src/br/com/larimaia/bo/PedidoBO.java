
package br.com.larimaia.bo;

import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.model.Pedido;
import br.com.larimaia.model.Produto;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class PedidoBO {
    private PedidoDAO pedidoDAO;

    public PedidoBO() {
        pedidoDAO = new PedidoDAO();
    }

    public PedidoBO(PedidoDAO pedidoDAO) {
        this.pedidoDAO = pedidoDAO;
    }
    
    public Integer salvar(Pedido pedido) throws ServiceException {

//        if (pedido.getDescricao().isEmpty()) {
//            throw new ServiceException("Campo descrição é obrigatório!");
//        }
//
//        if (pedido.getValor().isNaN()) {
//            throw new ServiceException("Campo telefone é obrigatório!");
//        }

        int i;
        i = pedidoDAO.salvar(pedido);
        return i;
    }

    public void excluir(Integer id) {
        pedidoDAO.excluir(id);
    }

    public Pedido buscarPorId(Integer id) {
        return pedidoDAO.buscarPorId(id);
    }

    public List<Pedido> buscarTodos() {
        return pedidoDAO.buscarTodos();
    }
}
