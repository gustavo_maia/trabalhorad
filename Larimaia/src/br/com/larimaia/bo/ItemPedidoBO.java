
package br.com.larimaia.bo;

import br.com.larimaia.dao.ItemPedidoDAO;
import br.com.larimaia.model.ItemPedido;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class ItemPedidoBO {
    private ItemPedidoDAO itemPedidoDAO;

    public ItemPedidoBO() {
        itemPedidoDAO = new ItemPedidoDAO();
    }

    public ItemPedidoBO(ItemPedidoDAO itemPedidoDAO) {
        this.itemPedidoDAO = itemPedidoDAO;
    }
    
    public void salvar(ItemPedido itemPedido) throws ServiceException {

        if (itemPedido.getQuantidade() == null) {
            throw new ServiceException("Campo quantidade é obrigatório!");
        }

        if (itemPedido.getValor().isNaN()) {
            throw new ServiceException("Campo valor é obrigatório!");
        }

        itemPedidoDAO.salvar(itemPedido);

    }

    public void excluir(Integer id) {
        itemPedidoDAO.excluir(id);
    }

    public List<ItemPedido> buscarPorIdPedido(Integer id) {
        return itemPedidoDAO.buscarPorIdPedido(id);
    }

    public List<ItemPedido> buscarTodos() {
        return itemPedidoDAO.buscarTodos();
    }
}
