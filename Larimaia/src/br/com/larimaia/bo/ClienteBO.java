
package br.com.larimaia.bo;

import br.com.larimaia.dao.ClienteDAO;
import br.com.larimaia.model.Cliente;
import br.com.larimaia.util.ServiceException;
import java.util.List;

public class ClienteBO {
    private ClienteDAO clienteDAO;

    public ClienteBO() {
        clienteDAO = new ClienteDAO();
    }

    public ClienteBO(ClienteDAO clienteDAO) {
        this.clienteDAO = clienteDAO;
    }
    
    public void salvar(Cliente cliente) throws ServiceException {

        if (cliente.getNome().isEmpty()) {
            throw new ServiceException("Campo nome é obrigatório!");
        }

        if (cliente.getTelefone().isEmpty()) {
            throw new ServiceException("Campo telefone é obrigatório!");
        }

        if (cliente.getEmail().isEmpty()) {
            throw new ServiceException("Campo email é obrigatório!");
        }
        clienteDAO.salvar(cliente);

    }

    public void excluir(Integer id) {
        clienteDAO.excluir(id);
    }

    public Cliente buscarPorId(Integer id) {
        return clienteDAO.buscarPorId(id);
    }

    public List<Cliente> buscarTodos() {
            return clienteDAO.buscarTodos();
    }
}
