
package br.com.larimaia.view;

import br.com.larimaia.model.Produto;
import java.util.Collection;

public interface CadProdutoView {
    interface CadProdutoViewListener{
        void salvar(Produto p);
        void excluir(Produto p);
    }
    
    public void addListener(CadProdutoViewListener listener);
    
    void sucesso(String mensagem);
    
    void populaListaProdutos(Collection produtos);
}
