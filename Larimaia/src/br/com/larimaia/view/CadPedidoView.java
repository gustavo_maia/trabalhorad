

package br.com.larimaia.view;

import br.com.larimaia.model.Cliente;
import br.com.larimaia.model.Endereco;
import br.com.larimaia.model.ItemPedido;
import br.com.larimaia.model.Pedido;
import br.com.larimaia.model.Produto;
import br.com.larimaia.model.TipoEvento;
import java.util.Collection;
import java.util.List;

public interface CadPedidoView {
    interface CadPedidoViewListener{
        void salvarPedido(Pedido p);
        void salvarEndereco(Endereco e, Pedido p);
        void salvarItemPedido(ItemPedido ip);
        List<ItemPedido> listaItemPedido(int id);
        void excluir(Pedido p);
    }
    
    public void addListener(CadPedidoViewListener listener);
    
    void populaComboCliente(List<Cliente> lista);
    
    void populaComboProduto(List<Produto> lista);
    
    void populaComboTipoEvento(List<TipoEvento> lista);
    
    void sucesso(String msg);
    
    void populaTabela(List<ItemPedido> lista);
    
    void populaListaPedidos(List<Pedido> lista);
    
}
