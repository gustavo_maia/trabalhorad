
package br.com.larimaia.view;

import br.com.larimaia.model.Cliente;
import java.util.Collection;

public interface CadClienteView {
    
    interface CadClienteViewListener{
        void salvar(Cliente c);
        void excluir(Cliente c);
    }
    
    public void addListener(CadClienteViewListener listener);
    
    void sucesso(String mensagem);
    
    void populaListaClientes(Collection clientes);
}
