
package br.com.larimaia.presenter;

import br.com.larimaia.bo.ClienteBO;
import br.com.larimaia.model.Cliente;
import br.com.larimaia.util.ServiceException;
import br.com.larimaia.view.CadClienteView;
import br.com.larimaia.view.CadClienteView.CadClienteViewListener;
import br.com.larimaia.viewImpl.CadClienteViewImpl;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CadClientePresenter implements CadClienteViewListener {
    
    CadClienteView view = new CadClienteViewImpl();
    ClienteBO bo;
    
    public CadClientePresenter(){
        this.view.addListener(this);
        bo = new ClienteBO();
        
        this.view.populaListaClientes(bo.buscarTodos());
    }
    public CadClienteView getView() {
	return view;
    }

    @Override
    public void salvar(Cliente c) {
        try {
            bo.salvar(c);
        } catch (ServiceException ex) {
            Logger.getLogger(CadClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.view.sucesso("Cliente salvo com sucesso");
        this.view.populaListaClientes(bo.buscarTodos());
    }

    @Override
    public void excluir(Cliente c) {
        bo.excluir(c.getIdCliente());
        this.view.sucesso("Cliente excluido com sucesso");
        this.view.populaListaClientes(bo.buscarTodos());
    }
}
