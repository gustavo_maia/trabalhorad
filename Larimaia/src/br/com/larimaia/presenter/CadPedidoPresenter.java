
package br.com.larimaia.presenter;

import br.com.larimaia.bo.ClienteBO;
import br.com.larimaia.bo.EnderecoBO;
import br.com.larimaia.bo.ItemPedidoBO;
import br.com.larimaia.bo.PedidoBO;
import br.com.larimaia.bo.ProdutoBO;
import br.com.larimaia.bo.TipoEventoBO;
import br.com.larimaia.model.Endereco;
import br.com.larimaia.model.ItemPedido;
import br.com.larimaia.model.Pedido;
import br.com.larimaia.util.ServiceException;
import br.com.larimaia.view.CadPedidoView;
import br.com.larimaia.view.CadPedidoView.CadPedidoViewListener;
import br.com.larimaia.viewImpl.CadPedidoViewImpl;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CadPedidoPresenter implements CadPedidoViewListener{
    
    CadPedidoView view = new CadPedidoViewImpl();
    PedidoBO pedidoBO;
    TipoEventoBO tipoEventoBO;
    ClienteBO clienteBO;
    ProdutoBO produtoBO;
    EnderecoBO enderecoBO;
    ItemPedidoBO itemPedidoBO;

    public CadPedidoPresenter() {
        
        this.view.addListener(this);
        
        pedidoBO = new PedidoBO();
        tipoEventoBO = new TipoEventoBO();
        clienteBO = new ClienteBO();
        produtoBO = new ProdutoBO();
        enderecoBO = new EnderecoBO();
        itemPedidoBO = new ItemPedidoBO();
        
        this.view.populaListaPedidos(pedidoBO.buscarTodos());
        this.view.populaComboCliente(clienteBO.buscarTodos());
        this.view.populaComboProduto(produtoBO.buscarTodos());
        this.view.populaComboTipoEvento(tipoEventoBO.buscarTodos());
        
    }
    
    public CadPedidoView getView() {
	return view;
    }

    @Override
    public void salvarPedido(Pedido p) {
        try {
            int i = pedidoBO.salvar(p);
            List<ItemPedido> lisIP = p.getListaItemPedido();
            for(ItemPedido ip : lisIP){
                ip.setIdPedido(i);
                salvarItemPedido(ip);
            }
            this.view.sucesso("Pedido realizado com sucesso");
        } catch (ServiceException ex) {
            Logger.getLogger(CadClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void salvarEndereco(Endereco e, Pedido p) {
        try {
            int i = enderecoBO.salvar(e);
            p.setIdEndereco(i);
            salvarPedido(p);
        } catch (ServiceException ex) {
            Logger.getLogger(CadClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void salvarItemPedido(ItemPedido ip) {
        try {
            itemPedidoBO.salvar(ip);
        } catch (ServiceException ex) {
            Logger.getLogger(CadClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.view.populaListaPedidos(pedidoBO.buscarTodos());
        
    }

    @Override
    public List<ItemPedido> listaItemPedido(int id) {
        return itemPedidoBO.buscarPorIdPedido(id);
    }

    @Override
    public void excluir(Pedido p) {
        itemPedidoBO.excluir(p.getIdPedido());
        pedidoBO.excluir(p.getIdPedido());
        enderecoBO.excluir(p.getEndereco().getIdEndereco());
        this.view.sucesso("Pedido excluído com sucesso!");
        this.view.populaListaPedidos(pedidoBO.buscarTodos());
    }
    
}
