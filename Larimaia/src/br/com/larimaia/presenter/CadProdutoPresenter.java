
package br.com.larimaia.presenter;

import br.com.larimaia.bo.ProdutoBO;
import br.com.larimaia.model.Produto;
import br.com.larimaia.util.ServiceException;
import br.com.larimaia.view.CadProdutoView;
import br.com.larimaia.view.CadProdutoView.CadProdutoViewListener;
import br.com.larimaia.viewImpl.CadProdutoViewImpl;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CadProdutoPresenter implements CadProdutoViewListener {
    
    CadProdutoView view = new CadProdutoViewImpl();
    ProdutoBO bo;
 
    public CadProdutoPresenter(){
        this.view.addListener(this);
        bo = new ProdutoBO();
        
        this.view.populaListaProdutos(bo.buscarTodos());
    }
    public CadProdutoView getView() {
	return view;
    }

    @Override
    public void salvar(Produto p) {
        try {
            bo.salvar(p);
        } catch (ServiceException ex) {
            Logger.getLogger(CadProdutoPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.view.sucesso("Produto salvo com sucesso");
        this.view.populaListaProdutos(bo.buscarTodos());
    }

    @Override
    public void excluir(Produto p) {
        bo.excluir(p.getIdProduto());
        this.view.sucesso("Produto excluído com sucesso");
        this.view.populaListaProdutos(bo.buscarTodos());
    }
    
}