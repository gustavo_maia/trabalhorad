
package br.com.larimaia.util;

public class ServiceException extends Exception {
    public ServiceException(String msg) {
        super(msg);
    }
}
