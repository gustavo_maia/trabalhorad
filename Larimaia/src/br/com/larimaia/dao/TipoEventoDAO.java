
package br.com.larimaia.dao;

import br.com.larimaia.model.TipoEvento;
import br.com.larimaia.util.ConexaoUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TipoEventoDAO {
    Connection conexao;
    
    public TipoEventoDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public TipoEvento buscarPorId(Integer id) {
        String sql = "select * from tipoevento where idtipoevento=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            if (resultado.next()) {
                //Instancia de cliente
                TipoEvento tip = new TipoEvento();

                //Atribuindo dados do resultado no objeto tipoEvento
                tip.setIdTipoEvento(id);
                tip.setDescricao(resultado.getString("descricao"));
                preparadorSQL.close();
                return tip;
            } else {
                return null;
            }
        } catch (SQLException ex) {

            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<TipoEvento> buscarTodos() {
        String sql = "select * from tipoevento order by idtipoevento";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<TipoEvento> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de TipoEvento
                TipoEvento tip = new TipoEvento();

                //Atribuindo dados do resultado no objeto TipoEvento
                tip.setIdTipoEvento(resultado.getInt("idtipoevento"));
                tip.setDescricao(resultado.getString("descricao"));
                //Adicionando cliente na lista
                lista.add(tip);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public void salvar(TipoEvento tipoEvento) {
        if (tipoEvento.getIdTipoEvento()== 0) {
            cadastrar(tipoEvento);
        } else {
            alterar(tipoEvento);
        }
    }

    public void cadastrar(TipoEvento tipoEvento) {
        String sql = "insert  into tipoevento (descricao) values (?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, tipoEvento.getDescricao());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void alterar(TipoEvento tipoEvento) {
        String sql = "update tipoevento set descricao=? where idtipoevento=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, tipoEvento.getDescricao());
            preparadorSQL.setInt(2, tipoEvento.getIdTipoEvento());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluir(Integer id) {
        String sql = "delete from tipoevento where idtipoevento=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
