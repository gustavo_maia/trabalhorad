
package br.com.larimaia.dao;

import br.com.larimaia.model.Endereco;
import br.com.larimaia.util.ConexaoUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EnderecoDAO {
    Connection conexao;
    
    public EnderecoDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public Endereco buscarPorId(Integer id) {
        String sql = "select * from enderecoentrega where idEndereco=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            if (resultado.next()) {
                //Instancia de Endereco
                Endereco end = new Endereco();

                //Atribuindo dados do resultado no objeto Endereco
                end.setIdEndereco(id);
                end.setCep(resultado.getString("cep"));
                end.setCidade(resultado.getString("cidade"));
                end.setBairro(resultado.getString("bairro"));
                end.setNumero(resultado.getInt("numero"));
                end.setEstado(resultado.getString("estado"));
                end.setRua(resultado.getString("rua"));
                preparadorSQL.close();
                return end;
            } else {
                return null;
            }
        } catch (SQLException ex) {

            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<Endereco> buscarTodos() {
        String sql = "select * from enderecoentrega order by idEndereco";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Endereco> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de cliente
                Endereco end = new Endereco();

                //Atribuindo dados do resultado no objeto Endereco
                end.setIdEndereco(resultado.getInt("idProduto"));
                end.setCep(resultado.getString("cep"));
                end.setCidade(resultado.getString("cidade"));
                end.setBairro(resultado.getString("bairro"));
                end.setNumero(resultado.getInt("numero"));
                end.setEstado(resultado.getString("estado"));
                end.setRua(resultado.getString("rua"));
                //Adicionando Endereco na lista
                lista.add(end);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public Integer salvar(Endereco endereco) {
        if (endereco.getIdEndereco()== 0) {
            int i = 0;
            i = cadastrar(endereco);
            return i;
        } else {
            alterar(endereco);
            return 0;
        }
    }

    public Integer cadastrar(Endereco endereco) {
        String sql = "insert  into enderecoentrega (bairro, cidade, cep, estado, rua, numero) values (?,?,?,?,?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparadorSQL.setString(1, endereco.getBairro());
            preparadorSQL.setString(2, endereco.getCidade());
            preparadorSQL.setString(3, endereco.getCep());
            preparadorSQL.setString(4, endereco.getEstado());
            preparadorSQL.setString(5, endereco.getRua());
            preparadorSQL.setInt(6, endereco.getNumero());
            preparadorSQL.execute();
            ResultSet rs = preparadorSQL.getGeneratedKeys();
            int key=0;
            while (rs.next()) {
                key = rs.getInt(1);
            }
            preparadorSQL.close();
            return key;
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }

    public void alterar(Endereco endereco) {
        String sql = "update enderecoentrega set bairro=?, cidade=?, cep=?, estado=?, rua=?, numero=? where idendereco=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, endereco.getBairro());
            preparadorSQL.setString(2, endereco.getCidade());
            preparadorSQL.setString(3, endereco.getCep());
            preparadorSQL.setString(4, endereco.getEstado());
            preparadorSQL.setString(5, endereco.getRua());
            preparadorSQL.setInt(6, endereco.getNumero());
            preparadorSQL.setInt(7, endereco.getIdEndereco());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluir(Integer id) {
        String sql = "delete from enderecoentrega where idEndereco=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
