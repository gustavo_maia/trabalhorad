
package br.com.larimaia.dao;

import br.com.larimaia.model.Cliente;
import br.com.larimaia.util.ConexaoUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteDAO {

    Connection conexao;
    
    public ClienteDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public Cliente buscarPorId(Integer id) {
        String sql = "select * from cliente where idCliente=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            if (resultado.next()) {
                //Instancia de cliente
                Cliente cli = new Cliente();

                //Atribuindo dados do resultado no objeto cliente
                cli.setIdCliente(id);
                cli.setNome(resultado.getString("nome"));
                cli.setTelefone(resultado.getString("telefone"));
                cli.setEmail(resultado.getString("email"));
                preparadorSQL.close();
                return cli;
            } else {
                return null;
            }
        } catch (SQLException ex) {

            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<Cliente> buscarTodos() {
        String sql = "select * from cliente order by idCliente";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Cliente> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de cliente
                Cliente cli = new Cliente();

                //Atribuindo dados do resultado no objeto cliente
                cli.setIdCliente(resultado.getInt("idCliente"));
                cli.setNome(resultado.getString("nome"));
                cli.setTelefone(resultado.getString("telefone"));
                cli.setEmail(resultado.getString("email"));
                //Adicionando cliente na lista
                lista.add(cli);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public void salvar(Cliente cliente) {
        if (cliente.getIdCliente()== 0) {
            cadastrar(cliente);
        } else {
            alterar(cliente);
        }
    }

    public void cadastrar(Cliente cliente) {
        String sql = "insert  into cliente (nome,telefone, email) values (?,?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, cliente.getNome());
            preparadorSQL.setString(2, cliente.getTelefone());
            preparadorSQL.setString(3, cliente.getEmail());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void alterar(Cliente cliente) {
        String sql = "update cliente set nome=? ,telefone=? ,email=? where idCliente=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, cliente.getNome());
            preparadorSQL.setString(2, cliente.getTelefone());
            preparadorSQL.setString(3, cliente.getEmail());
            preparadorSQL.setInt(4, cliente.getIdCliente());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluir(Integer id) {
        String sql = "delete from cliente where idCliente=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
