
package br.com.larimaia.dao;

import br.com.larimaia.model.ItemPedido;
import br.com.larimaia.util.ConexaoUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemPedidoDAO {
    Connection conexao;
    
    public ItemPedidoDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public List<ItemPedido> buscarPorIdPedido(Integer id) {
        String sql = "select * from itempedido where idpedido=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<ItemPedido> lista = new ArrayList<>();
            while (resultado.next()) {
                
                //Instancia de ItemPedido
                ItemPedido ipe = new ItemPedido();
                
                //Atribuindo dados do resultado no objeto itemPedido
                ipe.setIdItemPedido(id);
                ipe.setQuantidade(resultado.getInt("quantidade"));
                ipe.setValor(resultado.getDouble("valor"));
                ipe.setIdPedido(resultado.getInt("idpedido"));
                ipe.setIdProduto(resultado.getInt("idproduto"));
                
                lista.add(ipe);
            }
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(ItemPedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<ItemPedido> buscarTodos() {
        String sql = "select * from itempedido order by idItemPedido";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<ItemPedido> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de ItemPedido
                ItemPedido ipe = new ItemPedido();

                //Atribuindo dados do resultado no objeto produto
                ipe.setIdItemPedido(resultado.getInt("idPedido"));
                ipe.setQuantidade(resultado.getInt("quantidade"));
                ipe.setValor(resultado.getDouble("valor"));
                ipe.setIdPedido(resultado.getInt("idpedido"));
                ipe.setIdProduto(resultado.getInt("idproduto"));
                //Adicionando produto na lista
                lista.add(ipe);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(ItemPedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public void salvar(ItemPedido itemPedido) {
        if (itemPedido.getIdItemPedido()== 0) {
            cadastrar(itemPedido);
        } else {
            alterar(itemPedido);
        }
    }

    public void cadastrar(ItemPedido itemPedido) {
        String sql = "insert  into itemPedido (quantidade, valor, idpedido, idproduto) values (?,?,?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, itemPedido.getQuantidade());
            preparadorSQL.setDouble(2, itemPedido.getValor());
            preparadorSQL.setInt(3, itemPedido.getPedido().getIdPedido());
            preparadorSQL.setInt(4, itemPedido.getProduto().getIdProduto());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ItemPedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void alterar(ItemPedido itemPedido) {
        String sql = "update itemPedido set quantidade=?, valor=?, idpedido=?, idproduto=? where iditempedido=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, itemPedido.getQuantidade());
            preparadorSQL.setDouble(2, itemPedido.getValor());
            preparadorSQL.setInt(3, itemPedido.getPedido().getIdPedido());
            preparadorSQL.setInt(4, itemPedido.getProduto().getIdProduto());
            preparadorSQL.setInt(5, itemPedido.getIdItemPedido());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ItemPedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluir(Integer id) {
        String sql = "delete from itempedido where idpedido=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ItemPedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
