
package br.com.larimaia.model;

import br.com.larimaia.bo.PedidoBO;
import br.com.larimaia.bo.ProdutoBO;
import br.com.larimaia.dao.PedidoDAO;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ItemPedido {
    
    IntegerProperty idItemPedido, quantidade;
    Produto produto;
    Pedido pedido;
    DoubleProperty valor;

    public ItemPedido() {
        idItemPedido = new SimpleIntegerProperty();
        quantidade = new SimpleIntegerProperty();
        produto = new Produto();
        pedido = new Pedido();
        valor = new SimpleDoubleProperty();
    }
    
    //Getters e Setters para produto
    public Produto getProduto() { return produto;}
    public void setProduto(Produto produto) {this.produto = produto;}
    
    //Getters e Setters para pedido
    public Pedido getPedido() { return pedido;}
    public void setPedido(Pedido pedido) {this.pedido = pedido;}
    
    //Getters e Setters para valor
    public final Double getValor(){return valor.get();}
    public final void setValor(Double value){valor.set(value);}
    public DoubleProperty valorProperty() {return valor;}
    
    //Getters e Setters para idTipoEvento
    public final Integer getIdItemPedido(){return idItemPedido.get();}
    public final void setIdItemPedido(Integer value){idItemPedido.set(value);}
    public IntegerProperty idItemPedidoProperty() {return idItemPedido;}
    
    //Getters e Setters para quantidade
    public final Integer getQuantidade(){return quantidade.get();}
    public final void setQuantidade(Integer value){quantidade.set(value);}
    public IntegerProperty quantidadeProperty() {return quantidade;}
    
    public void setIdPedido(Integer id){
        PedidoBO bo = new PedidoBO();
        this.setPedido(bo.buscarPorId(id));
    }
    
    public void setIdProduto(Integer id){
        ProdutoBO bo = new ProdutoBO();
        this.setProduto(bo.buscarPorId(id));
    }
    
}
