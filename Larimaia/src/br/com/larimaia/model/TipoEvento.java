
package br.com.larimaia.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TipoEvento {
    IntegerProperty idTipoEvento;
    StringProperty descricao;

    public TipoEvento() {
        idTipoEvento = new SimpleIntegerProperty();
        descricao = new SimpleStringProperty();
    }
    
    //Getters e Setters para idTipoEvento
    public final Integer getIdTipoEvento(){return idTipoEvento.get();}
    public final void setIdTipoEvento(Integer value){idTipoEvento.set(value);}
    public IntegerProperty idTipoEventoProperty() {return idTipoEvento;}

    //Getters e Setters para descricao
    public final String getDescricao(){return descricao.get();}
    public final void setDescricao(String value){descricao.set(value);}
    public StringProperty descricaoProperty() {return descricao;}
    
    @Override
    public String toString() {
        return this.getDescricao();
    }
    
}
