package br.com.larimaia.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Cliente {

    IntegerProperty idCliente = new SimpleIntegerProperty();
    StringProperty nome = new SimpleStringProperty();
    StringProperty telefone = new SimpleStringProperty();
    StringProperty email = new SimpleStringProperty();

    //Getters e Setters para idCliente
    public final Integer getIdCliente() {
        return idCliente.get();
    }

    public final void setIdCliente(Integer value) {
        idCliente.set(value);
    }

    public IntegerProperty idClienteProperty() {
        return idCliente;
    }

    //Getters e Setters para nome
    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    //Getters e Setters para telefone
    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    //Getters e Setters para email
    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

}
