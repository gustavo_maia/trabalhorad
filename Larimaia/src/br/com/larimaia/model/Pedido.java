
package br.com.larimaia.model;

import br.com.larimaia.bo.ClienteBO;
import br.com.larimaia.bo.EnderecoBO;
import br.com.larimaia.bo.TipoEventoBO;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Pedido {
    
    IntegerProperty idPedido;
    Cliente cliente;
    Endereco endereco;
    List<ItemPedido> listaItemPedido;
    TipoEvento tipoEvento;
    StringProperty origemPedido, indicacao, cerimonial, observacao;
    ObjectProperty<LocalDate> dataPedido, dataHoraEvento;
    DoubleProperty valor;

    public Pedido() {
        listaItemPedido = new ArrayList();
        endereco = new Endereco();
        cliente = new Cliente();
        idPedido = new SimpleIntegerProperty();
        origemPedido = new SimpleStringProperty();
        indicacao = new SimpleStringProperty();
        cerimonial = new SimpleStringProperty();
        observacao = new SimpleStringProperty();
        dataPedido = new SimpleObjectProperty<>();
        dataHoraEvento = new SimpleObjectProperty<>();
        valor = new SimpleDoubleProperty();
    }
    
    //Getters e Setters para idPedido
    public final Integer getIdPedido(){return idPedido.get();}
    public final void setIdPedido(Integer value){idPedido.set(value);}
    public IntegerProperty idPedidoProperty() {return idPedido;}
    
    //Getters e Setters para origemPedido
    public final String getOrigemPedido(){return origemPedido.get();}
    public final void setOrigemPedido(String value){origemPedido.set(value);}
    public StringProperty origemPedidoProperty() {return origemPedido;}
    
    //Getters e Setters para indicacao
    public final String getIndicacao(){return indicacao.get();}
    public final void setIndicacao(String value){indicacao.set(value);}
    public StringProperty indicacaoProperty() {return indicacao;}
    
    //Getters e Setters para cerimonial
    public final String getCerimonial(){return cerimonial.get();}
    public final void setCerimonial(String value){cerimonial.set(value);}
    public StringProperty cerimonialProperty() {return cerimonial;}
    
    //Getters e Setters para observacao
    public final String getObservacao(){return observacao.get();}
    public final void setObservacao(String value){observacao.set(value);}
    public StringProperty observacaoProperty() {return observacao;}
    
    //Getters e Setters para dataPedido
    public LocalDate getDataPedido() {return dataPedido.get();}
    public void setDataPedido(LocalDate value) {dataPedido.set(value);}
    public ObjectProperty dataPedidoProperty() {return dataPedido;}
    
    //Getters e Setters para dataHoraEvento
    public LocalDate getDataHoraEvento() {return dataHoraEvento.get();}
    public void setDataHoraEvento(LocalDate value) {dataHoraEvento.set(value);}
    public ObjectProperty dataHoraEventoProperty() {return dataHoraEvento;}
    
    //Getters e Setters para valor
    public double getValor() {return valor.get();}
    public void setValor(double value) {valor.set(value);}
    public DoubleProperty valorProperty() {return valor;}

    //Getters e Setters para cliente
    public Cliente getCliente() { return cliente;}
    public void setCliente(Cliente cliente) {this.cliente = cliente;}
    
    //Getters e Setters para tipoEvento
    public TipoEvento getTipoEvento() { return tipoEvento;}
    public void setTipoEvento(TipoEvento tipoEvento) {this.tipoEvento = tipoEvento;}
    
    //Getters e Setters para endereco
    public Endereco getEndereco() { return endereco;}
    public void setEndereco(Endereco endereco) {this.endereco = endereco;}
    
    //Getters e Setters para listaItemPedido
    public List<ItemPedido> getListaItemPedido() { return listaItemPedido;}
    public void setListaItemPedido(List<ItemPedido> listaItemPedido) {this.listaItemPedido = listaItemPedido;}
    
    public void setIdCliente(Integer id){
        ClienteBO bo = new ClienteBO();
        this.setCliente(bo.buscarPorId(id));
    }
    
    public void setIdEndereco(Integer id){
        EnderecoBO bo = new EnderecoBO();
        this.setEndereco(bo.buscarPorId(id));
    }
    
    public void setIdTipoEvento(Integer id){
        TipoEventoBO bo = new TipoEventoBO();
        this.setTipoEvento(bo.buscarPorId(id));
    }
}
