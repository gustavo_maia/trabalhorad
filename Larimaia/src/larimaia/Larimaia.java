package larimaia;

import br.com.larimaia.presenter.CadClientePresenter;
import br.com.larimaia.presenter.CadPedidoPresenter;
import br.com.larimaia.presenter.CadProdutoPresenter;
import java.net.URL;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Larimaia extends Application {

    CadClientePresenter cadClientePresenter;
    CadProdutoPresenter cadProdutoPresenter;
    CadPedidoPresenter cadPedidoPresenter;

    private Button botCli, botPro, botPed;
    private Scene mainScene;
    private HBox layout;
    private BorderPane border;

    @Override
    public void start(final Stage stage) {

        layout = new HBox();
        layout.setPadding(new Insets(15, 12, 15, 12));
        layout.setStyle("-fx-background-color: #336699;");
        layout.setSpacing(10);

        BorderPane border = new BorderPane();
        border.setTop(layout);

        cadClientePresenter = new CadClientePresenter();
        cadProdutoPresenter = new CadProdutoPresenter();
        cadPedidoPresenter = new CadPedidoPresenter();

        //StackPane root = new StackPane();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        stage.setX(primaryScreenBounds.getMinX());
        stage.setY(primaryScreenBounds.getMinY());
        stage.setWidth(primaryScreenBounds.getWidth());
        stage.setHeight(primaryScreenBounds.getHeight());

        layout.setAlignment(Pos.TOP_CENTER);

        mainScene = new Scene(border, 500, 200);

        URL url = this.getClass().getResource("/br/com/larimaia/util/estiloLari.css");
        if (url == null) {
            System.out.println("Resource not found. Aborting.");
            System.exit(-1);
        }
        final String css = url.toExternalForm();
        mainScene.getStylesheets().add(css);
        stage.setScene(mainScene);

        botCli = new Button("Gerenciar Clientes");
        botCli.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                Scene cena = new Scene((Parent) cadClientePresenter.getView(), 500, 200);
                cena.getStylesheets().add(css);
                stage.setScene(cena);
            }
        });

        botPro = new Button("Gerenciar Produtos");
        botPro.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                Scene cena = new Scene((Parent) cadProdutoPresenter.getView(), 500, 200);
                cena.getStylesheets().add(css);
                stage.setScene(cena);
            }
        });

        botPed = new Button("Realizar Pedido");
        botPed.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                Scene cena = new Scene((Parent) cadPedidoPresenter.getView(), 500, 200);
                cena.getStylesheets().add(css);
                stage.setScene(cena);
            }
        });

        layout.getChildren().addAll(botCli, botPed, botPro);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
